### Omnipay-Peachpayments

#### Installation via composer
Add the below in your `composer.json` file:
```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://niel_casi@bitbucket.org/niel_casi/omnipay-peachpayments.git"
    }
]
```